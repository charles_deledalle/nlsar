# NL-SAR

NL-SAR Toolbox provides a collection of tools for the estimation of multi-modal
SAR images with non-local filters. Beyond estimation, NL-SAR Toolbox provides a
suite of tools to manipulate SAR images. There are 5 ways to interact with
NL-SAR Toolbox:
* in command line
* with Matlab
* with Python
* with IDL
* as a dynamic library ({\it e.g.} to use it from a C program).

NL-SAR support the following formats:
* RAT formats [read and write],
* PolSARpro formats [read and write],
* XIMA formats [read all and write \texttt{cxf}].
* TIFF formats (include Sentinel-1) [read only and write mono-channel].
* ENVI formats [read only and write mono-channel].
* CEOS formats (include ALOS-PALSAR) [read only].

Beyond file manipulation, the core functionnalities of NL-SAR Toolbox is to
implement the NL-SAR Algorithm for speckle noise reduction. NL-SAR is a general
method that builds extended non-local neighborhoods for denoising amplitude,
polarimetric and/or interferometric SAR images. These neighborhoods are defined
on the basis of pixel similarity as evaluated by multi-channel comparison of
patches. Several non-local estimations are performed and the best one is locally
selected to form a single restored image with good preservation of radar
structures and discontinuities.

## Installation

Configure and compile NL-SAR by typing in a shell prompt:
```bash
./configure
make
sudo make install
```
To install interfaces for Python, Matlab, IDL, or if you do not have super user
privileges, please refer to the documentation you can download from:
https://www.charles-deledalle.fr/pages/nlsar

## Documentation

Please refer to the documentation here:
https://www.charles-deledalle.fr/pages/nlsar

## Frequently asked questions (FAQ)

Please refer to the documentation you can download from:
https://www.charles-deledalle.fr/pages/nlsar

## License

This software is a computer program whose purpose is to provide a suite of tools
to manipulate SAR images.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software. You can use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and, more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
