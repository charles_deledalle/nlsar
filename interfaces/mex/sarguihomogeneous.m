function noise = sarguihomogeneous(filename)
%SARGUIHOMOGENEOUS
%%
%% sarguihomogeneous.m: An experimental GUI interface for
%% extracting homogeneous regions
%%
%% This file is part of NL-SAR Toolbox version 0.9.
%%
%% Copyright Charles-Alban Deledalle (2016)
%% Email charles-alban.deledalle@math.u-bordeaux.fr
%%
%% This software is a computer program whose purpose is to provide a
%% suite of tools to manipulate SAR images.
%%
%% This software is governed by the CeCILL license under French law and
%% abiding by the rules of distribution of free software. You can use,
%% modify and/ or redistribute the software under the terms of the CeCILL
%% license as circulated by CEA, CNRS and INRIA at the following URL
%% "http://www.cecill.info".
%%
%% As a counterpart to the access to the source code and rights to copy,
%% modify and redistribute granted by the license, users are provided only
%% with a limited warranty and the software's author, the holder of the
%% economic rights, and the successive licensors have only limited
%% liability.
%%
%% In this respect, the user's attention is drawn to the risks associated
%% with loading, using, modifying and/or developing or reproducing the
%% software by the user in light of its specific status of free software,
%% that may mean that it is complicated to manipulate, and that also
%% therefore means that it is reserved for developers and experienced
%% professionals having in-depth computer knowledge. Users are therefore
%% encouraged to load and test the software's suitability as regards their
%% requirements in conditions enabling the security of their systems and/or
%% data to be ensured and, more generally, to use and operate it in the
%% same conditions as regards security.
%%
%% The fact that you are presently reading this means that you have had
%% knowledge of the CeCILL license and that you accept its terms.
%%
%%
%% Started on  Wed Jul 24 16:00:24 2013 Charles-Alban Deledalle
%% Last update Fri Jul 15 08:38:32 2016 Charles-Alban Deledalle

    if nargin < 1
        [fn, pn] = uigetfile('*.rat', 'Open as');
        if fn == 0
            return;
        end
        filename = [ pn '/' fn ];
    else
        pn = pwd;
    end
    if isnumeric(filename)
        ima = filename;
    else
        if ~exist(filename)
            error(sprintf('%s: no such file or directory'));
        end
        [M, N, D] = sarinfo(filename);
        xoffset = floor((M - min(M, 512)) / 2);
        yoffset = floor((N - min(N, 512)) / 2);
        M = min(M, 512);
        N = min(N, 512);
        ima = sarread(filename, double(xoffset), double(yoffset), double(M), double(N));
    end
    [~, ~, N, M] = size(ima);

    h = openfig('sarguihomogeneous.fig', 'new', 'invisible');
    set(h,'HitTest','off');
    set(gcf, 'units','normalized','outerposition',[0 0 1 1]);
    g = guihandles(h);

    set(gcf, 'CurrentAxes', g.axes1);
    axis off
    axis image

    set(gcf, 'CurrentAxes', g.axes11);
    axis off
    axis image

    state = 'up';
    zone = 0;
    i1 = 0;
    j1 = 0;
    i2 = 0;
    j2 = 0;
    k1 = 0;
    l1 = 0;
    k2 = 0;
    l2 = 0;
    noise = zeros(0,0,0,0);

    Ws = [48 40 32];
    ga = [g.axes2, g.axes3, g.axes4, ...
          g.axes5, g.axes6, g.axes7, ...
          g.axes8, g.axes9, g.axes10, ...
         ];
    for l = 1:3
        W = Ws(l);
        if M < W || N < W
            continue
        end
        hom = sarhomogeneous(ima, W);
        [~, idx] = sort(hom(:), 'descend');
        for k = 1:3
            if 1+(k-1)*(W*W) > length(idx)
                continue
            end
            [i, j] = ind2sub([N, M], idx(1+(k-1)*(W*W)));

            %axes(ga((l-1)*3+k));
            set(gcf, 'CurrentAxes', ga((l-1)*3+k));
            sarshow(ima(:, :, ...
                        mod(i-1 + (1:W) - 1, N) + 1, ...
                        mod(j-1 + (1:W) - 1, M) + 1));
            xlim([0 W]+0.5);
            ylim([0 W]+0.5);

            set(get(gca,'Children'), 'ButtonDownFcn', ...
                              @(gco, event) refresh_mainvisu(i, j, W, W));
            if k == 1 && l == 1
                refresh_mainvisu(i, j, W, W);
            end
        end
    end
    set(g.pushbutton1, 'Callback', @(gco, event, dumy) save());
    set(g.pushbutton2, 'Callback', @(gco, event, dumy) quit());
    set(h, 'Visible', 'on');
    waitfor(h);
    if nargout == 0
        clear noise;
    end

    function refresh_mainvisu(i, j, W1, W2)
        i1 = max(i - 128 + floor(W1/2), 1);
        j1 = max(j - 128 + floor(W2/2), 1);
        i2 = min(i1 + 2*128, N);
        j2 = min(j1 + 2*128, M);
        i1 = max(i2 - 2*128, 1);
        j1 = max(j2 - 2*128, 1);

        i1 = max(min(i1, N), 1);
        i2 = max(min(i2, N), 1);
        j1 = max(min(j1, M), 1);
        j2 = max(min(j2, M), 1);

        [~, ~, N, M] = size(ima);
        set(gcf, 'CurrentAxes', g.axes1);
        hold off
        sarshow(ima(:, :, ...
                    (i1:i2), ...
                    (j1:j2)));
        xlim([0 i2-i1+1]+0.5);
        ylim([0 j2-j1+1]+0.5);

        hold on
        k1 = i - i1;
        l1 = j - j1;
        k2 = k1 + W1;
        l2 = l1 + W2;
        zone = plot([k1 k2 k2 k1 k1], [l1 l1 l2 l2 l1], 'r');

        noise = ima(:, :, ...
                    mod(i-1 + (1:W1) - 1, N) + 1, ...
                    mod(j-1 + (1:W2) - 1, M) + 1);

        set(get(g.axes1,'Children'), 'ButtonDownFcn', ...
                          @(gco, event) new_zone(gco, 'down'));
        set(h, 'WindowButtonMotionFcn', ...
               @(gco,y) new_zone(gco, 'motion'));
        set(g.figure1, 'WindowButtonUpFcn', ...
                       @(gco, event) new_zone(gco, 'up'));
    end

    function new_zone(gco, type)
        switch type
          case 'down'
            state = 'down';
            point1 = get(g.axes1, 'CurrentPoint');
            k1 = point1(1, 1);
            l1 = point1(1, 2);
          case 'motion'
            if strcmp(state, 'down')
                delete(zone);
                point2 = get(g.axes1,'CurrentPoint');
                k2 = point2(1, 1);
                l2 = point2(1, 2);
                set(gcf, 'CurrentAxes', g.axes1);
                zone = plot([k1 k2 k2 k1 k1], [l1 l1 l2 l2 l1], 'r');
            end
          case 'up'
            if strcmp(state, 'down')
                i = i1 + round(min(k1, k2));
                j = j1 + round(min(l1, l2));
                W1 = floor(abs(k2 - k1)) + 1;
                W2 = floor(abs(l2 - l1)) + 1;

                refresh_mainvisu(i, j, W1, W2);
                set(gcf, 'CurrentAxes', g.axes11);
                sarshow(noise);
                xlim([0 size(noise, 3)]+0.5);
                ylim([0 size(noise, 4)]+0.5);
                set(get(gca,'Children'), 'ButtonDownFcn', ...
                                  @(x, y) refresh_mainvisu(i, j, W1, W2));

                state = 'up';
            end
        end
    end

    function save()
        [fn, pnn] = uiputfile('*.rat', 'Save as', [pn '/' 'noise.rat']);
        if fn == 0
            return;
        end
        pn = pnn;
        sarwrite(noise, [pn '/' fn]);
    end

    function quit()
        close(h);
    end
end