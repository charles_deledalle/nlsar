/*
** ioceos.c: I/O of SAR images in CEOS SAR formats
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:53:26 2013 Charles-Alban Deledalle
** Last update Fri Jul 22 17:36:56 2016 Charles-Alban Deledalle
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include "tools/sarerror.h"
#include "tools/sarprintf.h"
#include "sardata.h"
#include "ioceos.h"

static void swapbyte(char* bytes, int N, unsigned int S)
{
  unsigned long int i;
  int k;
  char tmp;

  for (i = 0; i < S; ++i)
    for (k = 0; k < N/2; ++k)
      {
	tmp = bytes[i * N + k];
	bytes[i * N + k] = bytes[i * N + N - 1 - k];
	bytes[i * N + N - 1 - k] = tmp;
      }
}

static sardata* ceosread_header_(FILE* f, sardata* sar)
{
  int w, nbytes, d, s, byte_per_group;
  char dummy[64];

  if (!sar)
    sar = sardata_alloc();

  fseek(f, 16, SEEK_SET);
  if (fread(&dummy, sizeof(char), 12, f) == 12
      && strncmp(dummy, "CEOS-SAR-CCT", 12))
    {
      sarerror_msg("Unexpected control document number %s", dummy);
      return NULL;
    }
  fseek(f, 180, SEEK_SET);
  if (fscanf(f, " %6d%6d", &w, &nbytes) != 2)
    {
      sarerror_msg("Invalid image size");
      return NULL;
    }
  fseek(f, 216, SEEK_SET);
  if (fscanf(f, "%4d", &s) != 1)
    {
      sarerror_msg("Invalid number of bits per sample/pixel");
      return NULL;
    }
  byte_per_group = s / 8;
  if (fscanf(f, "   %1d", &s) != 1)
    {
      sarerror_msg("Invalid number of samples per data group");
      return NULL;
    }
  byte_per_group *= s;
  if (fscanf(f, "%4d", &s) != 1)
    {
      sarerror_msg("Invalid number of bytes per data group");
      return NULL;
    }
  if (byte_per_group != s)
    {
      sarerror_msg("Byte per data group is inconsistent");
      return NULL;
    }
  fseek(f, 232, SEEK_SET);
  if (fscanf(f, "%4d", &d) != 1)
    {
      sarerror_msg("Cannot extract number fo polarization channels");
      return NULL;
    }
  if (d != 1)
    {
      sarerror_msg("Cannot deal yet with more than 1 polarization channel");
      return NULL;
    }
  fseek(f, 268, SEEK_SET);
  if (fread(&dummy, sizeof(char), 4, f) == 4
      && strncmp(dummy, "BSQ ", 4))
    {
      sarerror_msg("Unexpected interleaving indicator %s", dummy);
      return NULL;
    }
  sar->M = nbytes / byte_per_group;
  sar->N = w;
  sar->D = d;
  return sar;
}

sardata* ceosread_header(const char* filename, sardata* sar)
{
  FILE* f;
  if (!(f = fopen(filename, "rb")))
    {
      sarerror_perror();
      return NULL;
    }
  sar = ceosread_header_(f, sar);
  fclose(f);
  if (!sar)
    return NULL;
  return sar;
}

sardata* ceosread_extract(const char* filename, sardata* sar,
			  long int xoffset, long int yoffset,
			  long int width, long int height)
{
  FILE* f;
  unsigned long int target_size;
  long int i, j, owidth, oheight;
  char type[256];
  char type_code[64];
  void* buf = 0;

  if (!(f = fopen(filename, "rb")))
    {
      sarerror_perror();
      return NULL;
    }
  if (!(sar = ceosread_header_(f, sar)))
    return NULL;
  fseek(f, 400, SEEK_SET);
  if (fread(&type, sizeof(char), 28, f) != 28)
    {
      sarerror_msg("Cannot read SAR data format type identifier", type);
      return NULL;
    }
  if (fread(&type_code, sizeof(char), 4, f) != 4)
    {
      sarerror_msg("Cannot read SAR data format type code", type);
      return NULL;
    }

  if (xoffset + width > sar->M || yoffset + height > sar->N)
    {
      sarerror_msg("Limits out of bounds");
      return NULL;
    }
  if (width < 0)
    width = sar->M - xoffset;
  if (height < 0)
    height = sar->N - yoffset;
  owidth = sar->M;
  oheight = sar->N;
  sar->M = width;
  sar->N = height;
  target_size = width * height * sar->D * sar->D;
  if (!(sar->array = realloc(sar->array, target_size * sizeof(float complex))))
    {
      sarerror_msg_perror("Cannot allocate array field");
      return NULL;
    }

  if (!strncmp(type_code, "I*1 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(char)), SEEK_END);
      buf = malloc(width * sizeof(char));
      fseek(f, yoffset * owidth * sizeof(char), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(char), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(char), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(char), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(char), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((char*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "I*2 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(short)), SEEK_END);
      buf = malloc(width * sizeof(short));
      fseek(f, yoffset * owidth * sizeof(short), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(short), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(short), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(short), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(short), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((short*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "I*4 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(int)), SEEK_END);
      buf = malloc(width * sizeof(int));
      fseek(f, yoffset * owidth * sizeof(int), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(int), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(int), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(int), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(int), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((int*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "I*2 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(short)), SEEK_END);
      buf = malloc(width * sizeof(short));
      fseek(f, yoffset * owidth * sizeof(short), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(short), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(short), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(short), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(short), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((short*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "IU1 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(unsigned char)), SEEK_END);
      buf = malloc(width * sizeof(unsigned char));
      fseek(f, yoffset * owidth * sizeof(unsigned char), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(unsigned char), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(unsigned char), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(unsigned char), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(unsigned char), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((unsigned char*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "IU2 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(unsigned short)), SEEK_END);
      buf = malloc(width * sizeof(unsigned short));
      fseek(f, yoffset * owidth * sizeof(unsigned short), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(unsigned short), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(unsigned short), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(unsigned short), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(unsigned short), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((unsigned short*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "IU4 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(unsigned int)), SEEK_END);
      buf = malloc(width * sizeof(unsigned int));
      fseek(f, yoffset * owidth * sizeof(unsigned int), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(unsigned int), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(unsigned int), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(unsigned int), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(unsigned int), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((unsigned int*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "R*4 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(float)), SEEK_END);
      buf = malloc(width * sizeof(float));
      fseek(f, yoffset * owidth * sizeof(float), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(float), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(float), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(float), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(float), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((float*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "R*8 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(double)), SEEK_END);
      buf = malloc(width * sizeof(double));
      fseek(f, yoffset * owidth * sizeof(double), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(double), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(double), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(double), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(double), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((double*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "C*4 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(complex float)), SEEK_END);
      buf = malloc(width * sizeof(complex float));
      fseek(f, yoffset * owidth * sizeof(complex float), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(complex float), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(complex float), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(complex float), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(complex float), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((complex float*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "C*8 ", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(complex double)), SEEK_END);
      buf = malloc(width * sizeof(complex double));
      fseek(f, yoffset * owidth * sizeof(complex double), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(complex double), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(complex double), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(complex double), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(complex double), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((complex double*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "CI*1", 4) || !strncmp(type_code, "CI*2", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(complex char)), SEEK_END);
      buf = malloc(width * sizeof(complex char));
      fseek(f, yoffset * owidth * sizeof(complex char), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(complex char), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(complex char), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(complex char), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(complex char), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((complex char*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "CI*4", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(complex short)), SEEK_END);
      buf = malloc(width * sizeof(complex short));
      fseek(f, yoffset * owidth * sizeof(complex short), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(complex short), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(complex short), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(complex short), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(complex short), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((complex short*) buf)[i];
	}
      free(buf);
    }
  if (!strncmp(type_code, "CI*8", 4))
    {
      fseek(f, -(oheight * owidth) * (sizeof(complex int)), SEEK_END);
      buf = malloc(width * sizeof(complex int));
      fseek(f, yoffset * owidth * sizeof(complex int), SEEK_CUR);
      for (j = 0; j < height; ++j)
	{
	  fseek(f, xoffset * sizeof(complex int), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(complex int), width, f) != width)
	    {
	      sarerror_msg_perror("Unexpected number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(complex int), SEEK_CUR);
	  swapbyte((char*) buf, sizeof(complex int), width);
	  for (i = 0; i < width; ++i)
	    SARDATA_ACCESS(sar, i, j, 0, 0) =
	      (complex float) ((complex int*) buf)[i];
	}
      free(buf);
    }
  if (!buf)
    {
      sarerror_msg_perror("Unimplemented SAR data type '%s'", type);
      return NULL;
    }
  fclose(f);
  return sar;
}

sardata* ceosread(const char* filename, sardata* sar)
{
  return ceosread_extract(filename, sar, 0, 0, -1, -1);
}
