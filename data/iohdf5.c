/*
** iohdf5.c: I/O of SAR images in HDF5 SAR formats
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:53:26 2013 Charles-Alban Deledalle
** Last update Fri Jul 22 17:36:56 2016 Charles-Alban Deledalle
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#ifdef LIBHDF5
# include <hdf5.h>
#endif
#include "tools/sarerror.h"
#include "tools/sarprintf.h"
#include "sardata.h"
#include "iohdf5.h"

#ifdef LIBHDF5
static sardata* hdf5read_header_(hid_t f, sardata* sar)
{
  hid_t       d;

  if ((d = H5Dopen(f, "/S01/B001", H5P_DEFAULT)) < 0)
    {
      sarerror_msg("Cannot find '/S01/B001' dataset");
      return NULL;
    }
  H5Dclose(d);
  sarerror_msg("Read of HFD5 format not implemented yet");
  return NULL;
  sar->M = 0;
  sar->N = 0;
  sar->D = 1;
  return sar;
}
#endif

sardata* hdf5read_header(const char* filename, sardata* sar)
{
#ifdef LIBHDF5
  hid_t       f;

  if (!(f = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT)))
    {
      sarerror_perror();
      return NULL;
    }
  sar = hdf5read_header_(f, sar);
  H5Fclose(f);
  return sar;
#else
  filename = filename;
  sar = sar;
  return NULL;
#endif
}

sardata* hdf5read_extract(const char* filename, sardata* sar,
			  long int xoffset, long int yoffset,
			  long int width, long int height)
{
#ifdef LIBHDF5
  hid_t       f;
  unsigned long int target_size;
  long int owidth, oheight;

  if (!(f = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT)))
    {
      sarerror_perror();
      return NULL;
    }
  if (!(sar = hdf5read_header_(f, sar)))
    return NULL;

  if (xoffset + width > sar->M || yoffset + height > sar->N)
    {
      sarerror_msg("Limits out of bounds");
      return NULL;
    }
  if (width < 0)
    width = sar->M - xoffset;
  if (height < 0)
    height = sar->N - yoffset;
  owidth = sar->M;
  oheight = sar->N;
  sar->M = width;
  sar->N = height;
  target_size = width * height * sar->D * sar->D;
  if (!(sar->array = realloc(sar->array, target_size * sizeof(float complex))))
    {
      sarerror_msg_perror("Cannot allocate array field");
      return NULL;
    }

  //FIXME

  H5Fclose(f);
  return sar;
#else
  filename = filename;
  sar = sar;
  xoffset = xoffset;
  yoffset = yoffset;
  width = width;
  height = height;
  return NULL;
#endif
}

sardata* hdf5read(const char* filename, sardata* sar)
{
  return hdf5read_extract(filename, sar, 0, 0, -1, -1);
}
