/*
** iosafe.c: I/O of SAR images in TIFF formats
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:53:26 2013 Charles-Alban Deledalle
** Last update Fri Jul 22 17:36:56 2016 Charles-Alban Deledalle
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#ifdef LIBTIFF
# include <tiffio.h>
#endif
#include "tools/sarerror.h"
#include "tools/sarprintf.h"
#include "sardata.h"
#include "iotiff.h"

#ifdef LIBTIFF
static sardata* tiffread_header_(TIFF* f, sardata* sar)
{
  uint32 w, h;
  if (!sar)
    sar = sardata_alloc();

  TIFFGetField(f, TIFFTAG_IMAGEWIDTH,   &w);
  TIFFGetField(f, TIFFTAG_IMAGELENGTH,  &h);
  sar->M = w;
  sar->N = h;
  sar->D = 1;
  return sar;
}
#endif

sardata* tiffread_header(const char* filename, sardata* sar)
{
#ifdef LIBTIFF
  TIFF* f;
  if (!(f = TIFFOpen(filename, "rb")))
    {
  sarerror_perror();
  return NULL;
}
  sar = tiffread_header_(f, sar);
  TIFFClose(f);
  if (!sar)
    return NULL;
  return sar;
#else
  sarerror_msg("TIFF format required libtiff to be installed");
  sarerror_msg_msg("Please install libtiff, configure and recompile");
  return NULL;
#endif
}

sardata* tiffread_extract(const char* filename, sardata* sar,
			  long int xoffset, long int yoffset,
			  long int width, long int height)
{
#ifdef LIBTIFF
  TIFF* f;
  unsigned long int target_size;
  uint32 row, col;
  tdata_t buf;
  uint16 sf, bps, s;

  if (!(f = TIFFOpen(filename, "rb")))
    {
      sarerror_perror();
      return NULL;
    }
  sar = tiffread_header_(f, sar);

  if (xoffset + width > sar->M || yoffset + height > sar->N)
    {
      sarerror_msg("Limits out of bounds");
      return NULL;
    }
  if (width < 0)
    width = sar->M - xoffset;
  if (height < 0)
    height = sar->N - yoffset;
  sar->M = width;
  sar->N = height;
  target_size = width * height * sar->D * sar->D;
  if (!(sar->array = realloc(sar->array, target_size * sizeof(float complex))))
    {
      sarerror_msg_perror("Cannot allocate array field");
      return NULL;
    }

  TIFFGetField(f, TIFFTAG_SAMPLEFORMAT,  &sf);
  switch (sf)
    {
      case SAMPLEFORMAT_UINT:
      case SAMPLEFORMAT_INT:
      case SAMPLEFORMAT_IEEEFP:
      case SAMPLEFORMAT_COMPLEXINT:
      case SAMPLEFORMAT_COMPLEXIEEEFP:
	break;
      default:
	sarerror_msg("Sample format %d unsupported", s);
	return NULL;
    }
  TIFFGetField(f, TIFFTAG_SAMPLESPERPIXEL,  &s);
  if (s != 1)
    {
      sarerror_msg("Found %s samples per pixel, expected 1", s);
      return NULL;
    }
  TIFFGetField(f, TIFFTAG_PLANARCONFIG, &s);
  if (s != PLANARCONFIG_CONTIG)
    {
      sarerror_msg("Expect contiguous planar configuration");
      return NULL;
    }
  TIFFGetField(f, TIFFTAG_BITSPERSAMPLE,  &bps);
  switch (bps)
    {
      case 8:
      case 16:
      case 32:
      case 64:
	break;
      case 128:
	if (sf == SAMPLEFORMAT_COMPLEXIEEEFP)
	  break;
	sarerror_msg("Unexpected number of bits per sample for complex float format");
	return NULL;
      default:
	sarerror_msg("Unexpected number of bits per sample", s);
	return NULL;
    }
  if (!(buf = _TIFFmalloc(TIFFScanlineSize(f))))
    {
      sarerror_msg("Allocation failed");
      return NULL;
    }
  for (row = 0; row < height; ++row)
    {
      TIFFReadScanline(f, buf, yoffset + row, sf);
      if (sf == SAMPLEFORMAT_UINT && bps == 8)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((unsigned char*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_UINT && bps == 16)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((unsigned short*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_UINT && bps == 32)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((unsigned int*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_UINT && bps == 64)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((unsigned long*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_INT && bps == 8)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((char*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_INT && bps == 16)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((short*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_INT && bps == 32)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((int*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_INT && bps == 64)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((long*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_IEEEFP && bps < 32)
	{
	  sarerror_msg("Unexpected number of bits per sample for float fortmat", s);
	  return NULL;
	}
      if (sf == SAMPLEFORMAT_IEEEFP && bps == 32)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((float*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_IEEEFP && bps == 64)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((double*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_COMPLEXINT && bps == 8)
	{
	  sarerror_msg("Unexpected number of bits per sample for float fortmat", s);
	  return NULL;
	}
      if (sf == SAMPLEFORMAT_COMPLEXINT && bps == 16)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((complex char*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_COMPLEXINT && bps == 32)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((complex short*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_COMPLEXINT && bps == 64)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((complex int*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_COMPLEXIEEEFP && bps < 64)
	{
	  sarerror_msg("Unexpected number of bits per sample for complex float fortmat", s);
	  return NULL;
	}
      if (sf == SAMPLEFORMAT_COMPLEXIEEEFP && bps == 64)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((complex float*) buf)[xoffset + col];
      if (sf == SAMPLEFORMAT_COMPLEXIEEEFP && bps == 128)
	for (col = 0; col < width; ++col)
	  SARDATA_ACCESS(sar, col, row, 0, 0) =
	    (complex float) ((complex double*) buf)[xoffset + col];
    }
  _TIFFfree(buf);
  TIFFClose(f);
  return sar;
#else
  sarerror_msg("TIFF format required libtiff to be installed");
  sarerror_msg_msg("Please install libtiff, configure and recompile");
  return NULL;
#endif
}

sardata* tiffread(const char* filename, sardata* sar)
{
  return tiffread_extract(filename, sar, 0, 0, -1, -1);
}

int tiffwrite(const sardata* sar, const char* filename)
{
#ifdef LIBTIFF
  TIFF* f;
  uint32 row, col;
  float complex* buf;

  if (sar->D > 1)
    {
      sarerror_msg("Cannot export multi-dimensional SAR images to TIFF format");
      return 0;
    }
  if (!(f = TIFFOpen(filename, "wb")))
    {
      sarerror_perror();
      return 0;
    }
  TIFFSetField(f, TIFFTAG_IMAGEWIDTH,   sar->M);
  TIFFSetField(f, TIFFTAG_IMAGELENGTH,  sar->N);
  TIFFSetField(f, TIFFTAG_SAMPLEFORMAT,    SAMPLEFORMAT_COMPLEXIEEEFP);
  TIFFSetField(f, TIFFTAG_SAMPLESPERPIXEL, 1);
  TIFFSetField(f, TIFFTAG_PLANARCONFIG,    PLANARCONFIG_CONTIG);
  TIFFSetField(f, TIFFTAG_BITSPERSAMPLE,   64);
  if (!(buf = _TIFFmalloc(TIFFScanlineSize(f))))
    {
      sarerror_msg("Allocation failed");
      return 0;
    }
  for (row = 0; row < (uint32) sar->N; ++row)
    {
      for (col = 0; col < (uint32) sar->M; ++col)
	buf[col] = SARDATA_ACCESS(sar, col, row, 0, 0);
      TIFFWriteScanline(f, (tdata_t) buf, row, SAMPLEFORMAT_COMPLEXIEEEFP);
    }
  _TIFFfree(buf);
  TIFFClose(f);
  return 1;
#else
  sarerror_msg("TIFF format required libtiff to be installed");
  sarerror_msg_msg("Please install libtiff, configure and recompile");
  return 0;
#endif
}
