/*
** ioenvi.c: I/O of SAR images in ENVI formats
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:53:26 2013 Charles-Alban Deledalle
** Last update Fri Jul 22 17:36:56 2016 Charles-Alban Deledalle
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include "tools/sarerror.h"
#include "tools/sarprintf.h"
#include "sardata.h"
#include "ioenvi.h"

static void swapbyte(char* bytes, int N, unsigned int S)
{
  unsigned long int i;
  int k;
  char tmp;

  for (i = 0; i < S; ++i)
    for (k = 0; k < N/2; ++k)
      {
	tmp = bytes[i * N + k];
	bytes[i * N + k] = bytes[i * N + N - 1 - k];
	bytes[i * N + N - 1 - k] = tmp;
      }
}

static sardata* enviread_header_(FILE* f, sardata* sar)
{
  int w = -1, h = -1;
  char* line = NULL;
  size_t len = 0;
  if (!sar)
    sar = sardata_alloc();

  fseek(f, 0, SEEK_SET);
  while (getline(&line, &len, f) != -1)
    {
      if (sscanf(line, "samples = %d", &w) == 1)
	continue;
      if (sscanf(line, "lines   = %d", &h) == 1)
	continue;
    }
  free(line);
  if (w == -1 || h == -1)
    {
      sarerror_msg("Cannot extract row or col numbers");
      return NULL;
    }
  sar->M = w;
  sar->N = h;
  sar->D = 1;
  return sar;
}

sardata* enviread_header(const char* filename, sardata* sar)
{
  FILE* f;
  if (!(f = fopen(filename, "r")))
    {
      sarerror_perror();
      return NULL;
    }
  sar = enviread_header_(f, sar);
  fclose(f);
  if (!sar)
    return NULL;
  return sar;
}

sardata* enviread_extract(const char* filename, sardata* sar,
			  long int xoffset, long int yoffset,
			  long int width, long int height)
{
  FILE* f;
  unsigned long int target_size;
  long int i, j;
  long int owidth;
  int type = -1, order = -1;
  void* buf;
  char* line = NULL;
  size_t len = 0;
  char* fnraw;
  int fnlen;
  if (!(f = fopen(filename, "rb")))
    {
      sarerror_perror();
      return NULL;
    }
  if (!(sar = enviread_header_(f, sar)))
    return NULL;
  fseek(f, 0, SEEK_SET);
  while (getline(&line, &len, f) != -1)
    {
      if (sscanf(line, "data type = %d", &type) == 1)
	continue;
      if (sscanf(line, "byte order = %d", &order) == 1)
	continue;
    }
  free(line);
  fclose(f);
  if (type == -1 || order == -1)
    {
      sarerror_msg("Cannot extract data type or byte order");
      return NULL;
    }
  if (xoffset + width > sar->M || yoffset + height > sar->N)
    {
      sarerror_msg("Limits out of bounds");
      return NULL;
    }
  if (width < 0)
    width = sar->M - xoffset;
  if (height < 0)
    height = sar->N - yoffset;
  owidth = sar->M;
  sar->M = width;
  sar->N = height;

  fnraw = strdup(filename);
  fnlen = strlen(fnraw);
  fnraw[fnlen-4] = '\0';
  if (!(f = fopen(fnraw, "rb")))
    {
      fnraw[fnlen-4] = '.';
      sarerror_perror();
      return NULL;
    }
  fnraw[fnlen-4] = '.';
  free(fnraw);

  target_size = width * height * sar->D * sar->D;
  if (!(sar->array = realloc(sar->array, target_size * sizeof(float complex))))
    {
      sarerror_msg_perror("Cannot allocate array field");
      return NULL;
    }
  switch (type)
    {
      case 1:
	buf = malloc(width * sizeof(unsigned char));
	fseek(f, yoffset * owidth * sizeof(unsigned char), SEEK_CUR);
	break;
      case 2:
	buf = malloc(width * sizeof(short));
	fseek(f, yoffset * owidth * sizeof(short), SEEK_CUR);
	break;
      case 3:
	buf = malloc(width * sizeof(int));
	fseek(f, yoffset * owidth * sizeof(int), SEEK_CUR);
	break;
      case 4:
	buf = malloc(width * sizeof(float));
	fseek(f, yoffset * owidth * sizeof(float), SEEK_CUR);
	break;
      case 5:
	buf = malloc(width * sizeof(double));
	fseek(f, yoffset * owidth * sizeof(double), SEEK_CUR);
	break;
      case 6:
	buf = malloc(width * sizeof(complex float));
	fseek(f, yoffset * owidth * sizeof(complex float), SEEK_CUR);
	break;
      case 9:
	buf = malloc(width * sizeof(complex double));
	fseek(f, yoffset * owidth * sizeof(complex double), SEEK_CUR);
	break;
      case 12:
	buf = malloc(width * sizeof(unsigned short));
	fseek(f, yoffset * owidth * sizeof(unsigned short), SEEK_CUR);
	break;
      case 13:
	buf = malloc(width * sizeof(unsigned int));
	fseek(f, yoffset * owidth * sizeof(unsigned int), SEEK_CUR);
	break;
      case 14:
	buf = malloc(width * sizeof(long));
	fseek(f, yoffset * owidth * sizeof(long), SEEK_CUR);
	break;
      case 15:
	buf = malloc(width * sizeof(unsigned long));
	fseek(f, yoffset * owidth * sizeof(unsigned long), SEEK_CUR);
	break;
      default:
	sarerror_msg("Data type %d not implemented yet", type);
	return NULL;
    }
  for (j = 0; j < height; ++j)
    switch (type)
      {
	case 1:
	  fseek(f, xoffset * sizeof(unsigned char), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(unsigned char), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(unsigned char), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(unsigned char), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((unsigned char*) buf)[i];
	    }
	  break;
	case 2:
	  fseek(f, xoffset * sizeof(short), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(short), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(short), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(short), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((short*) buf)[i];
	    }
	  break;
	case 3:
	  fseek(f, xoffset * sizeof(int), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(int), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(int), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(int), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((int*) buf)[i];
	    }
	  break;
	case 4:
	  fseek(f, xoffset * sizeof(float), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(float), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(float), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(float), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((float*) buf)[i];
	    }
	  break;
	case 5:
	  fseek(f, xoffset * sizeof(double), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(double), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(double), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(double), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((double*) buf)[i];
	    }
	  break;
	case 6:
	  fseek(f, xoffset * sizeof(complex float), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(complex float), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(complex float), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(complex float), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((complex float*) buf)[i];
	    }
	  break;
	case 9:
	  fseek(f, xoffset * sizeof(complex double), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(complex double), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(complex double), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(complex double), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((complex double*) buf)[i];
	    }
	  break;
	case 12:
	  fseek(f, xoffset * sizeof(unsigned short), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(unsigned short), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(unsigned short), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(unsigned short), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((unsigned short*) buf)[i];
	    }
	  break;
	case 13:
	  fseek(f, xoffset * sizeof(unsigned int), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(unsigned int), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(unsigned int), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(unsigned int), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((unsigned int*) buf)[i];
	    }
	  break;
	case 14:
	  fseek(f, xoffset * sizeof(long), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(long), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(long), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(long), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((long*) buf)[i];
	    }
	  break;
	case 15:
	  fseek(f, xoffset * sizeof(unsigned long), SEEK_CUR);
	  if ((long int) fread(buf, sizeof(unsigned long), width, f) != width)
	    {
	      sarerror_msg("Unexpeted number of elements");
	      return NULL;
	    }
	  fseek(f, (owidth - width - xoffset) * sizeof(unsigned long), SEEK_CUR);
	  if (order)
	    swapbyte((char*) buf, sizeof(unsigned long), width);
	  for (i = 0; i < width; ++i)
	    {
	      SARDATA_ACCESS(sar, i, j, 0, 0) =
		(complex float) ((unsigned long*) buf)[i];
	    }
	  break;
	default:
	sarerror_msg("Data type %d not implemented yet", type);
	return NULL;
      }
  free(buf);
  fclose(f);
  return sar;
}

sardata* enviread(const char* filename, sardata* sar)
{
  return enviread_extract(filename, sar, 0, 0, -1, -1);
}

int enviwrite(const sardata* sar, const char* filename)
{
  FILE* f;
  long int i, j;
  long int width, height;
  void* buf;
  char* fnraw;
  int fnlen;

  width = sar->M;
  height = sar->N;
  if (sar->D > 1)
    {
      sarerror_msg("Cannot export multi-dimensional SAR images to ENVI format");
      return 0;
    }
  if (!(f = fopen(filename, "wb")))
    {
      sarerror_perror();
      return 0;
    }
  fnlen = strlen(filename);
  if (fnlen <= 4)
    {
      sarerror_msg("Envi image filenames should contain at least 4 characters.");
      return 0;
    }
  fprintf(f, "ENVI\n");
  fprintf(f, "description = {\n"
	     "  File Exported from NLSAR Toolbox.}\n");
  fprintf(f, "samples = %ld\n", width);
  fprintf(f, "lines   = %ld\n", height);
  fprintf(f, "bands   = %d\n", 1);
  fprintf(f, "header offset = %d\n", 0);
  fprintf(f, "file type = ENVI Standard\n");
  fprintf(f, "data type = %d\n", 6); // complex float
  fprintf(f, "interleave = bsq\n");
  fprintf(f, "sensor type = Unknown\n");
  fprintf(f, "byte order = 0\n");
  fprintf(f, "wavelength unit = Unknown\n");
  fprintf(f, "complex function = Power\n");
  fclose(f);

  fnraw = strdup(filename);
  fnlen = strlen(fnraw);
  fnraw[fnlen-4] = '\0';
  if (!(f = fopen(fnraw, "wb")))
    {
      fnraw[fnlen-4] = '.';
      sarerror_perror();
      return 0;
    }
  fnraw[fnlen-4] = '.';
  free(fnraw);

  buf = malloc(width * sizeof(complex float));
  for (j = 0; j < height; ++j)
    {
      for (i = 0; i < width; ++i)
	((complex float*) buf)[i] =
	  SARDATA_ACCESS(sar, i, j, 0, 0);
      fwrite(buf, sizeof(complex float), width, f);
    }
  free(buf);
  fclose(f);
  return 1;
}
