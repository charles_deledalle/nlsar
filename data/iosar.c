/*
** iosar.c: I/O of SAR images files
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:53:11 2013 Charles-Alban Deledalle
** Last update Fri Jul 22 16:52:45 2016 Charles-Alban Deledalle
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __MINGW32__
# include <strings.h>
#endif
#include "tools/sarerror.h"
#include "tools/sarprintf.h"
#include "sardata.h"
#include "iorat.h"
#include "iobin.h"
#include "iosar.h"
#include "ioxima.h"
#include "iotiff.h"
#include "ioenvi.h"
#include "ioceos.h"
#include "iohdf5.h"

static const char* formatmsg =
  "Please, if '%s' is in amplitude or complex format, convert it in intensity "
  "with 'sarjoin' before doing any processing.\n"
  "Please ignore this message if you are using 'sarjoin'.";

sardata* sarread(const char* filename, sardata* sar)
{
  FILE* f;
  int length;
  char* fn = strdup(filename);
  sardata* res = NULL;
  length = strlen(filename);
  int swap = 0;

  if (length >= 6 && !strcasecmp(fn + length - 6, "[SWAP]"))
    {
      fn[length - 6] = '\0';
      length -= 6;
      swap = 1;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".rat"))
    {
      res = ratread(fn, sar);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".ima"))
    {
      res = imaread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".imw"))
    {
      res = imwread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".ims"))
    {
      res = imsread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".imf"))
    {
      res = imfread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".imd"))
    {
      res = imdread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".cxb"))
    {
      res = cxbread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".cxs"))
    {
      res = cxsread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".cxf"))
    {
      res = cxfread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!res && length >= 4 && !strcasecmp(fn + length - 4, ".cxd"))
    {
      res = cxdread(fn, sar, swap);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if ((length >= 4 && !strcasecmp(fn + length - 4, ".tif")) ||
      (length >= 5 && !strcasecmp(fn + length - 5, ".tiff")))
    {
      res = tiffread(fn, sar);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if ((length >= 4 && !strcasecmp(fn + length - 4, ".hdr")))
    {
      res = enviread(fn, sar);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if ((length >= 3 && !strcasecmp(fn + length - 3, ".h5")))
    {
      res = hdf5read(fn, sar);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if ((res = binread_header(fn, sar, swap)))
    {
      res = binread(fn, res, swap);
      free(fn);
      return res;
    }
  if ((res = ceosread_header(fn, sar)))
    {
      res = ceosread(fn, res);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!(f = fopen(fn, "rb")))
    {
      free(fn);
      sarerror_perror();
      return NULL;
    }
  fclose(f);
  sarerror_msg("Unknown file format");
  free(fn);
  return NULL;
}

sardata* sarread_extract(const char* filename, sardata* sar,
			 long int xoffset, long int yoffset,
			 long int width, long int height)
{
  FILE* f;
  int length;
  char* fn = strdup(filename);
  sardata* res;
  length = strlen(filename);
  int swap = 0;

  if (length >= 6 && !strcasecmp(fn + length - 6, "[SWAP]"))
    {
      fn[length - 6] = '\0';
      length -= 6;
      swap = 1;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".rat"))
    {
      res = ratread_extract(fn, sar,
			    xoffset, yoffset,
			    width, height);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".ima"))
    {
      res = imaread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".imw"))
    {
      res = imwread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".ims"))
    {
      res = imsread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".imf"))
    {
      res = imfread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".imd"))
    {
      res = imdread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".cxb"))
    {
      res = cxbread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".cxs"))
    {
      res = cxsread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".cxf"))
    {
      res = cxfread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".cxd"))
    {
      res = cxdread_extract(fn, sar, swap,
			    xoffset, yoffset,
			    width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if ((length >= 4 && !strcasecmp(fn + length - 4, ".tif")) ||
      (length >= 5 && !strcasecmp(fn + length - 5, ".tiff")))
    {
      res = tiffread_extract(fn, sar,
			     xoffset, yoffset,
			     width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if ((length >= 4 && !strcasecmp(fn + length - 4, ".hdr")))
    {
      res = enviread_extract(fn, sar,
			     xoffset, yoffset,
			     width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if ((length >= 3 && !strcasecmp(fn + length - 3, ".h5")))
    {
      res = hdf5read_extract(fn, sar,
			     xoffset, yoffset,
			     width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if ((res = binread_header(fn, sar, swap)))
    {
      res = binread_extract(fn, res, swap,
			    xoffset, yoffset,
			    width, height);
      free(fn);
      return res;
    }
  if ((res = ceosread_header(fn, sar)))
    {
      res = ceosread_extract(fn, res,
			     xoffset, yoffset,
			     width, height);
      sarprintf_warning(formatmsg, fn);
      free(fn);
      return res;
    }
  if (!(f = fopen(fn, "rb")))
    {
      free(fn);
      sarerror_perror();
      return NULL;
    }
  fclose(f);
  sarerror_msg("Unknown file format");
  free(fn);
  return res;
}

sardata* sarread_header(const char* filename, sardata* sar)
{
  FILE* f;
  int length;
  char* fn = strdup(filename);
  sardata* res = NULL;
  length = strlen(filename);
  int swap = 0;

  if (length >= 6 && !strcasecmp(fn + length - 6, "[SWAP]"))
    {
      fn[length - 6] = '\0';
      length -= 6;
      swap = 1;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".rat"))
    {
      res = ratread_header(fn, sar);
      free(fn);
      return res;
    }
  if (length >= 4 &&
      (!strcasecmp(fn + length - 4, ".ima") ||
       !strcasecmp(fn + length - 4, ".imw") ||
       !strcasecmp(fn + length - 4, ".ims") ||
       !strcasecmp(fn + length - 4, ".imf") ||
       !strcasecmp(fn + length - 4, ".imd") ||
       !strcasecmp(fn + length - 4, ".cxb") ||
       !strcasecmp(fn + length - 4, ".cxs") ||
       !strcasecmp(fn + length - 4, ".cxf") ||
       !strcasecmp(fn + length - 4, ".cxd")))
    {
      res = ximaread_header(fn, sar, swap);
      free(fn);
      return res;
    }
  if ((length >= 4 && !strcasecmp(fn + length - 4, ".tif")) ||
      (length >= 5 && !strcasecmp(fn + length - 5, ".tiff")))
    {
      res = tiffread_header(fn, sar);
      free(fn);
      return res;
    }
  if ((length >= 4 && !strcasecmp(fn + length - 4, ".hdr")))
    {
      res = enviread_header(fn, sar);
      free(fn);
      return res;
    }
  if ((length >= 3 && !strcasecmp(fn + length - 3, ".h5")))
    {
      res = hdf5read_header(fn, sar);
      free(fn);
      return res;
    }
  if ((res = binread_header(fn, sar, swap)))
    {
      free(fn);
      return res;
    }
  if ((res = ceosread_header(fn, sar)))
    {
      free(fn);
      return res;
    }
  if (!(f = fopen(fn, "rb")))
    {
      free(fn);
      sarerror_perror();
      return NULL;
    }
  fclose(f);
  sarerror_msg("Unknown file format");
  free(fn);
  return NULL;
}

int sarwrite(const sardata* sar, const char* filename)
{
  int length;
  char* fn = strdup(filename);
  int res;
  length = strlen(filename);
  int swap = 0;

  if (length >= 6 && !strcasecmp(fn + length - 6, "[SWAP]"))
    {
      fn[length - 6] = '\0';
      length -= 6;
      swap = 1;
    }
  if (length >= 4 && !strcasecmp(fn + length - 4, ".rat"))
    {
      res = ratwrite(sar, fn);
      free(fn);
      return res;
    }
  if (length >= 4 &&
      (!strcasecmp(fn + length - 4, ".ima") ||
       !strcasecmp(fn + length - 4, ".imw") ||
       !strcasecmp(fn + length - 4, ".ims") ||
       !strcasecmp(fn + length - 4, ".imf") ||
       !strcasecmp(fn + length - 4, ".imd") ||
       !strcasecmp(fn + length - 4, ".cxb") ||
       !strcasecmp(fn + length - 4, ".cxs") ||
       !strcasecmp(fn + length - 4, ".cxf") ||
       !strcasecmp(fn + length - 4, ".cxd")))
    {
      if (sar->D > 1 || strcasecmp(fn + length - 4, ".cxf"))
	{
	  sarerror_msg("Export to XIMA format is only for 1-dimensional data in cxf format");
	  res = 0;
	}
      else
	res = cxfwrite(sar, fn);
      free(fn);
      return res;
    }
  if ((length >= 4 && !strcasecmp(fn + length - 4, ".tif")) ||
      (length >= 5 && !strcasecmp(fn + length - 5, ".tiff")))
    {
      if (sar->D > 1)
	{
	  sarerror_msg("Export to TIFF format is only for 1-dimensional data");
	  res = 0;
	}
      else
	res = tiffwrite(sar, fn);
      free(fn);
      return res;
    }
  if ((length >= 4 && !strcasecmp(fn + length - 4, ".hdr")))
    {
      if (sar->D > 1)
	{
	  sarerror_msg("Export to ENVI format is only for 1-dimensional data");
	  res = 0;
	}
      else
	res = enviwrite(sar, fn);
      free(fn);
      return res;
    }
  if ((length >= 3 && !strcasecmp(fn + length - 3, ".h5")))
    {
      sarerror_msg("Export to HDF5 format is not implemented");
      free(fn);
      return 0;
    }
  res = binwrite(sar, fn, swap);
  free(fn);
  return res;
}

