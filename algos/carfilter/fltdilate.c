/*
** fltdiskdilate.c: implementation of the disk dilate filter
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 14:58:54 2013 Charles-Alban Deledalle
** Last update Wed Nov 20 18:05:05 2013 Charles-Alban Deledalle
*/

#include "data/fltdata.h"
#include "tools/mathtools.h"
#include "carfilter.h"

fltdata* fltdiskdilate(const fltdata* input, fltdata* output, int hW)
{
  int x, y, d, dx, dy, x_dx, y_dy;
  int M, N, D;

  M = input->M;
  N = input->N;
  D = input->D;
  if (input == output)
    output = fltdata_dup(input);
  else
    output = fltdata_copy(input, output);
  for (x = 0; x < M; ++x)
    for (y = 0; y < N; ++y)
      for (d = 0; d < D; ++d)
	for (dx = -hW; dx <= hW; ++dx)
	  for (dy = -hW; dy <= hW; ++dy)
	    if (!(dx == 0 && dy ==0) &&
		dx * dx + dy * dy < (hW + 0.5) * (hW + 0.5))
	      {
		x_dx = MOD(x + dx, M);
		y_dy = MOD(y + dy, N);
		if (FLTDATA_ACCESS(input, x_dx, y_dy, d) > FLTDATA_ACCESS(output, x, y, d))
		  FLTDATA_ACCESS(output, x, y, d) = FLTDATA_ACCESS(input, x_dx, y_dy, d);
	      }
  return output;
}

fltdata* fltdiskerode(const fltdata* input, fltdata* output, int hW)
{
  int x, y, d, dx, dy, x_dx, y_dy;
  int M, N, D;

  M = input->M;
  N = input->N;
  D = input->D;
  if (input == output)
    output = fltdata_dup(input);
  else
    output = fltdata_copy(input, output);
  for (x = 0; x < M; ++x)
    for (y = 0; y < N; ++y)
      for (d = 0; d < D; ++d)
	for (dx = -hW; dx <= hW; ++dx)
	  for (dy = -hW; dy <= hW; ++dy)
	    if (!(dx == 0 && dy ==0) &&
		dx * dx + dy * dy < (hW + 0.5) * (hW + 0.5))
	      {
		x_dx = MOD(x + dx, M);
		y_dy = MOD(y + dy, N);
		if (FLTDATA_ACCESS(input, x_dx, y_dy, d) < FLTDATA_ACCESS(output, x, y, d))
		  FLTDATA_ACCESS(output, x, y, d) = FLTDATA_ACCESS(input, x_dx, y_dy, d);
	      }
  return output;
}

fltdata* fltboxdilate(const fltdata* input, fltdata* output, int hW)
{
  int x, y, d, dx, dy, x_dx, y_dy;
  int M, N, D;

  M = input->M;
  N = input->N;
  D = input->D;
  if (input == output)
    output = fltdata_dup(input);
  else
    output = fltdata_copy(input, output);
  for (x = 0; x < M; ++x)
    for (y = 0; y < N; ++y)
      for (d = 0; d < D; ++d)
	for (dx = -hW; dx <= hW; ++dx)
	  for (dy = -hW; dy <= hW; ++dy)
	    if (!(dx == 0 && dy ==0))
	      {
		x_dx = MOD(x + dx, M);
		y_dy = MOD(y + dy, N);
		if (FLTDATA_ACCESS(input, x_dx, y_dy, d) > FLTDATA_ACCESS(output, x, y, d))
		  FLTDATA_ACCESS(output, x, y, d) = FLTDATA_ACCESS(input, x_dx, y_dy, d);
	      }
  return output;
}

fltdata* fltboxerode(const fltdata* input, fltdata* output, int hW)
{
  int x, y, d, dx, dy, x_dx, y_dy;
  int M, N, D;

  M = input->M;
  N = input->N;
  D = input->D;
  if (input == output)
    output = fltdata_dup(input);
  else
    output = fltdata_copy(input, output);
  for (x = 0; x < M; ++x)
    for (y = 0; y < N; ++y)
      for (d = 0; d < D; ++d)
	for (dx = -hW; dx <= hW; ++dx)
	  for (dy = -hW; dy <= hW; ++dy)
	    if (!(dx == 0 && dy ==0))
	      {
		x_dx = MOD(x + dx, M);
		y_dy = MOD(y + dy, N);
		if (FLTDATA_ACCESS(input, x_dx, y_dy, d) < FLTDATA_ACCESS(output, x, y, d))
		  FLTDATA_ACCESS(output, x, y, d) = FLTDATA_ACCESS(input, x_dx, y_dy, d);
	      }
  return output;
}
