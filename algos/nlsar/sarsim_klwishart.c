/*
** sarsim_klwishart.c: KL between data following a Wishart distribution
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:46:27 2013 Charles-Alban Deledalle
** Last update Fri Jul 22 19:19:44 2016 Charles-Alban Deledalle
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include "tools/sarerror.h"
#include "tools/sarprintf.h"
#include "tools/mathtools.h"
#include "tools/matrixtools.h"
#include "sarsim.h"
#include "sardiagload.h"

#define DATA_WISHART_C(data, k, l)		(((float complex*) (data))[(k) * D + (l)])
#define DATA_WISHART_Inv(data)			((float complex*) (data) + D * D)
#define KLDATA_WISHART_C(stats, i, j, k, l)	(DATA_WISHART_C(SARSIMDATA_ACCESS((stats), (i), (j)), (k), (l)))
#define KLDATA_WISHART_Inv(stats, i, j)		(DATA_WISHART_Inv(SARSIMDATA_ACCESS((stats), (i), (j))))

sarsimdata* kldata_wishart_create(int L, const sardata* sar)
{
  int M = sar->M;
  int N = sar->N;
  int D = sar->D;
  int i, j, k, l;
  sardata* sar_norm = NULL;
  float norm = 0;

#if defined(BLAS) && defined(LAPACK)
  long int info, longD = D;
  int ipiv[6];
  float complex work[6];
  int lwork = 6;
#endif

  sar_norm = sardiagload(sar, sar_norm, L);
  for (i = 0; i < M; ++i)
    for (j = 0; j < N; ++j)
      for (k = 0; k < D; ++k)
	norm += SARDATA_ACCESS(sar_norm, i, j, k, k);
  norm /= M * N;
  for (i = 0; i < M; ++i)
    for (j = 0; j < N; ++j)
      for (k = 0; k < D; ++k)
	for (l = 0; l < D; ++l)
	  SARDATA_ACCESS(sar_norm, i, j, k, l) /= norm;
  sarsimdata* res = malloc(sizeof(sarsimdata));
  if (!res)
    {
      sarerror_perror();
      return NULL;
    }
  res->M = M;
  res->N = N;
  res->size = 2 * D * D * sizeof(float complex);
  if (!(res->data = malloc(M * N * res->size)))
    {
      sarerror_perror();
      return NULL;
    }
  for (i = 0; i < M; ++i)
    for (j = 0; j < N; ++j)
      {
	memcpy(&KLDATA_WISHART_C(res, i, j, 0, 0),
	       &SARDATA_ACCESS(sar_norm, i, j, 0, 0),
	       D * D * sizeof(float complex));
	switch (D)
	  {
	    case 1:
	      *KLDATA_WISHART_Inv(res, i, j) = 1.0 / KLDATA_WISHART_C(res, i, j, 0, 0);
	    default:
#if defined(BLAS) && defined(LAPACK)
	      memcpy(KLDATA_WISHART_Inv(res, i, j),
		     &KLDATA_WISHART_C(res, i, j, 0, 0),
		     D * D * sizeof(float complex));
	      cgetrf_(&longD, &longD, KLDATA_WISHART_Inv(res, i, j), &longD, ipiv, &info);
	      cgetri_(&longD, KLDATA_WISHART_Inv(res, i, j), &longD, ipiv, work, &lwork, &info);
#else
	      sarerror_msg("KL distance required BLAS and LAPACK to be installed.");
	      sarerror_msg("Please install LAPACK, CBLAS, configure and recompile.");
	      exit(3);
#endif
	  }
      }
  sardata_free(sar_norm);
  return res;
}

sarsimdata* kldata_wishart_free(sarsimdata* sarsimdata)
{
  if (sarsimdata)
    {
      if (sarsimdata->data)
	free(sarsimdata->data);
      free(sarsimdata);
    }
  return NULL;
}

float lkl_wishart(int D, const void* C1, const void* C2)
{
  return
    crealf(trace_C1_C2(D, (float complex*)(DATA_WISHART_Inv(C1)), (float complex*)(C2))) +
    crealf(trace_C1_C2(D, (float complex*)(DATA_WISHART_Inv(C2)), (float complex*)(C1))) +
    - 2 * D;
}

const sarsimfuncs sarsim_klwishart =
  { &kldata_wishart_create,
    &kldata_wishart_free,
    &lkl_wishart };

float slkl_wishart(int D, const void* C1, const void* C2)
{
  return sqrtf(MAX(lkl_wishart(D, C1, C2), 0.0));
}

const sarsimfuncs sarsim_sklwishart =
  { &kldata_wishart_create,
    &kldata_wishart_free,
    &slkl_wishart };
