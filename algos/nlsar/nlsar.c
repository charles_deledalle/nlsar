/*
** nlsar.c: implementation of the NL-SAR filter
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:46:54 2013 Charles-Alban Deledalle
** Last update Fri Jul 22 19:28:00 2016 Charles-Alban Deledalle
*/

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#ifdef OMP
# include <omp.h>
#endif //!OMP
#include "tools/sarprintf.h"
#include "tools/sarwaitbar.h"
#include "data/sardata.h"
#include "tools/sarerror.h"
#include "tools/mathtools.h"
#include "tools/matrixtools.h"
#include "algos/carfilter/carfilter.h"
#include "algos/noisegen/noisegen.h"
#include "sarsim.h"
#include "sarsimstats.h"
#include "nlsar.h"
#include "spirale.h"
#include "phi.h"

#define OFFSET(s, k, x, y)			(((s)*(M*N*K)+(k)*(M*N)+(x)*N+(y))*nb_stats)
#define DIFF_OFFSET(s)				((s) * (Mext * Next))

static float compute_alpha(float cvar, float cmean)
{
  float alpha;

  alpha = MAX(cvar - cmean, 0);
  //alpha = fabsf(cvar - cmean);
  alpha = alpha / (alpha + cmean);
  return alpha;
}

#define STATS_w(stats, offset)			(stats)[(offset)]
#define STATS_w2(stats, offset)			(stats)[(offset) + 1]
#define STATS_wfrob2(stats, offset)		(stats)[(offset) + 2]
#define STATS_wspan2(stats, offset)		(stats)[(offset) + 3]
#define STATS_wspanfdx2(stats, offset)		(stats)[(offset) + 4]
#define STATS_wspanfdy2(stats, offset)		(stats)[(offset) + 5]
#define STATS_wspanbdx2(stats, offset)		(stats)[(offset) + 6]
#define STATS_wspanbdy2(stats, offset)		(stats)[(offset) + 7]
#define STATS_wdiag2(stats, offset, k)		(stats)[(offset) + 8 + (k)]
#define STATS_wdiag(stats, offset, D, k)	(stats)[(offset) + 8 + (D) + (k)]
#define STATS_woffdiag(stats, offset, D, i, j)	*((float complex*) ((stats) + (offset) + 8 + (D) + (D) + (i) * (i - 1) + 2 * (j)))

static sardata* nlsar_core(const sardata*		input,
			   sardata*			output,
			   const sarsimstats*		statssim,
			   sardata** const		guides,
			   const sarsimfuncs** const	simfunc,
			   sarsimstats** const		gstatssim,
			   int				G,
			   float			rho,
			   float			h,
			   int				verbose,
			   fltdata*       		outlook,
			   fltdata*       		outhW,
			   fltdata*       		outhP,
			   fltdata*       		outS,
			   fltdata*       		outG,
			   fltdata*       		outalpha,
			   int*				gl_cpt,
			   int				nb_windows,
			   void*			hwaitbar)
{
  sardata*	bguide;
  sarsimdata**	guidesarsim;
  float**	weights_spatial;
  int		M = input->M;
  int		N = input->N;
  int		D = input->D;
  int		Mext, Next;
  int		offset, offset0;
  int		k, l;
  int		i, j;
  int		cs;
  int		x, y, dx, dy, x_dx, y_dy;
  float		d, w;
  float	complex value;
  float		sum_w, sum_w2, span2, frob2;
  float*	sum_wstats;
  float*	map_stats;
  int		nb_stats;
  float*	diff;
  float		alpha, alpha2, alpha3, clook;
  int		percent;
  int		g;
  int		s;
  float       	L;
  int		hWmin;
  int		hWmax;
  int		Wmax;
  int		hPmin;
  int		hPmax;
  int		hP;
  int		hStep;
  int		K;
  int		S;
  int		pos_p_p, pos_p_m, pos_m_p, pos_m_m;
  float		eta2;
  float		zeta2;

  for (x = 0; x < M; ++x)
    for (y = 0; y < N; ++y)
      FLTDATA_ACCESS(outlook, x, y, 0) = 0;
  eta2  = rho * statssim->eta2;
  zeta2 = rho * statssim->zeta2;
  for (g = 0; g < G; ++g)
    {
      L     = gstatssim[g]->L;
      hWmin = gstatssim[g]->hWmin;
      hWmax = gstatssim[g]->hWmax;
      Wmax  = gstatssim[g]->Wmax;
      hPmin = gstatssim[g]->hPmin;
      hPmax = gstatssim[g]->hPmax;
      hStep = gstatssim[g]->hStep;
      K     = gstatssim[g]->K;
      S     = gstatssim[g]->S;

      { // Allocations
	guidesarsim = malloc(S * sizeof(sarsimdata*));
	bguide = sardata_dup(guides[g]);
	for (s = 0; s < S; ++s)
	  {
#pragma omp critical
	    {
	      if (s)
		bguide = sargausscar(guides[g], bguide, s);
	    }
	    if (!(guidesarsim[s] = simfunc[g]->create(L, bguide)))
	      return NULL;
	  }
	sardata_free(bguide);
	weights_spatial = malloc(K * sizeof(float*));
	for (k = 0; k < K; k += hStep)
	  {
	    hP = k+hPmin;
	    weights_spatial[k] = malloc((2 * hWmax * hWmax + 1) * sizeof(float));
	    for (i = 0; i <= 2 * hWmax * hWmax; ++i)
	      weights_spatial[k][i] = expf(-((float) i)/((2*hP+1)+0.5)/((2*hP+1)+0.5));
	  }
	// 8 stats + D diag2 + D diag + (D^2 - D)/2 complex off diag
	nb_stats = 8 + D + D * D;
	if (!(map_stats  = calloc(M * N * nb_stats, sizeof(float))) ||
	    !(sum_wstats = calloc(M * N * S * K * nb_stats, sizeof(float))))
	  {
	    sarerror_perror();
	    return NULL;
	  }
	for (x = 0; x < M; ++x)
	  for (y = 0; y < N; ++y)
	    {
	      offset0 = OFFSET(0, 0, x, y);
	      for (i = 0; i < D; ++i)
		for (j = 0; j <= i; ++j)
		  {
		    value = SARDATA_ACCESS(input, x, y, i, j);
		    if (i == j)
		      {
			STATS_wfrob2(map_stats, offset0)        += cabsf(value * value);
			STATS_wspan2(map_stats, offset0)        += cabsf(value);
			STATS_wdiag2(map_stats, offset0, i)      = cabsf(value * value);
			STATS_wdiag(map_stats, offset0, D, i)    = cabsf(value);
		      }
		    else
		      {
			STATS_wfrob2(map_stats, offset0)           += 2 * cabsf(value * value);
			STATS_woffdiag(map_stats, offset0, D, i, j) = value;
		      }
		  }
	    }
	for (x = 0; x < M; ++x)
	  for (y = 0; y < N; ++y)
	    {
	      offset0 = OFFSET(0, 0, x, y);
	      x_dx = MOD(x+1, M);
	      y_dy = MOD(y+1, N);
	      STATS_wspanfdx2(map_stats, offset0) =
		STATS_wspan2(map_stats, offset0) -
		STATS_wspan2(map_stats, OFFSET(0, 0, x_dx, y));
	      STATS_wspanfdy2(map_stats, offset0) =
		STATS_wspan2(map_stats, offset0) -
		STATS_wspan2(map_stats, OFFSET(0, 0, x, y_dy));
	      x_dx = MOD(x-1, M);
	      y_dy = MOD(y-1, N);
	      STATS_wspanbdx2(map_stats, offset0) =
		STATS_wspan2(map_stats, offset0) -
		STATS_wspan2(map_stats, OFFSET(0, 0, x_dx, y));
	      STATS_wspanbdy2(map_stats, offset0) =
		STATS_wspan2(map_stats, offset0) -
		STATS_wspan2(map_stats, OFFSET(0, 0, x, y_dy));
	    }
	for (x = 0; x < M; ++x)
	  for (y = 0; y < N; ++y)
	    {
	      offset0 = OFFSET(0, 0, x, y);
	      STATS_wspan2(map_stats, offset0) *=
		STATS_wspan2(map_stats, offset0);
	      STATS_wspanfdx2(map_stats, offset0) *=
		STATS_wspanfdx2(map_stats, offset0);
	      STATS_wspanfdy2(map_stats, offset0) *=
		STATS_wspanfdy2(map_stats, offset0);
	      STATS_wspanbdx2(map_stats, offset0) *=
		STATS_wspanbdx2(map_stats, offset0);
	      STATS_wspanbdy2(map_stats, offset0) *=
		STATS_wspanbdy2(map_stats, offset0);
	    }

	Mext = M+2*hPmax+1;
	Next = N+2*hPmax+1;
	diff = malloc(S * Mext * Next * sizeof(float));
      }
      {
	// Central pixel
	w = 1;
	for (k = 0; k < K; k += hStep)
	  for (s = 0; s < S; ++s)
	    for (x = 0; x < M; ++x)
	      for (y = 0; y < N; ++y)
		{
		  offset0 = OFFSET(0, 0, x, y);
		  offset  = OFFSET(s, k, x, y);
		  STATS_w(sum_wstats, offset) = w;
		  STATS_w2(sum_wstats, offset) = w * w;
		  for (i = 2; i < nb_stats; ++i)
		    sum_wstats[offset + i] = w * map_stats[offset0 + i];
		}
	for (x = 0; x < M; ++x)
	  for (y = 0; y < N; ++y)
	    {
	      FLTDATA_ACCESS(outhW, x, y, 0) = 1;
	      FLTDATA_ACCESS(outhP, x, y, 0) = hPmin;
	      FLTDATA_ACCESS(outS, x, y, 0) = 0;
	      for (i = 0; i < D; ++i)
		for (j = 0; j < i; ++j)
		  SARDATA_ACCESS(output, x, y, i, j) = SARDATA_ACCESS(input, x, y, i, j);
	    }

	// Other pixels
	l = 0;
	for (cs = hStep; cs <= hWmax; cs += hStep)
	  {
	    for (; l < gstatssim[g]->spirale_lengths[cs] - 1 && l < Wmax - 1; ++l)
	      {
#pragma omp atomic
		(*gl_cpt)++;
		dx = gstatssim[g]->spirale_shifts[l+1][0];
		dy = gstatssim[g]->spirale_shifts[l+1][1];
		// Compute difference
		for (s = 0; s < S; ++s)
		  {
		    for (x = -hPmax-1; x < M+hPmax; ++x)
		      {
			x_dx = MOD(x + dx, M);
			for (y = -hPmax-1; y < N+hPmax; ++y)
			  {
			    y_dy = MOD(y + dy, N);
			    diff[DIFF_OFFSET(s) + (x+1+hPmax) * Next + (y+1+hPmax)] =
			      simfunc[g]->lsarsim(D,
						  SARSIMDATA_ACCESS(guidesarsim[s], MOD(x, M), MOD(y, N)),
						  SARSIMDATA_ACCESS(guidesarsim[s], x_dx, y_dy));
			  }
		      }
		    // Compute commulative sums
		    for (y = 1; y < Next; ++y)
		      diff[DIFF_OFFSET(s) + y] += diff[DIFF_OFFSET(s) + y-1];
		    for (x = 1; x < Mext; ++x)
		      diff[DIFF_OFFSET(s) + x * Next] += diff[DIFF_OFFSET(s) + (x-1) * Next];
		    for (x = 1; x < Mext; ++x)
		      for (y = 1; y < Next; ++y)
			diff[DIFF_OFFSET(s) + x * Next + y] +=
			  + diff[DIFF_OFFSET(s) + (x-1) * Next + y]
			  + diff[DIFF_OFFSET(s) + x     * Next + (y-1)]
			  - diff[DIFF_OFFSET(s) + (x-1) * Next + (y-1)];

		    // Perform K NL-means
		    for (k = 0; k < K; k += hStep)
		      {
			hP = k+hPmin;
			for (x = 0; x < M; ++x)
			  {
			    pos_p_p = DIFF_OFFSET(s) + (x+1+hPmax+hP)   * Next + (1+hPmax+hP);
			    pos_p_m = DIFF_OFFSET(s) + (x+1+hPmax+hP)   * Next + (1+hPmax-hP-1);
			    pos_m_p = DIFF_OFFSET(s) + (x+1+hPmax-hP-1) * Next + (1+hPmax+hP);
			    pos_m_m = DIFF_OFFSET(s) + (x+1+hPmax-hP-1) * Next + (1+hPmax-hP-1);
			    x_dx = MOD(x + dx, M);
			    for (y = 0; y < N; ++y)
			      {
				y_dy = MOD(y + dy, N);

				// Extract weights for current patch size
				d =
				  + diff[pos_p_p + y]
				  - diff[pos_p_m + y]
				  - diff[pos_m_p + y]
				  + diff[pos_m_m + y];
				if (isnan(d) || isinf(d))
				  continue;
				w = phi(d, SARSIMSTATS_ACCESS(gstatssim[g], s, k, l), h);
				w *= weights_spatial[k][dx*dx+dy*dy];

				// Accumulation target
				{
				  offset  = OFFSET(s, k, x, y);
				  offset0 = OFFSET(0, 0, x_dx, y_dy);
				  STATS_w(sum_wstats, offset)  += w;
				  STATS_w2(sum_wstats, offset) += w * w;
				  for (i = 2; i < nb_stats; ++i)
				    sum_wstats[offset + i] += w * map_stats[offset0 + i];
				}

				// Accumulation candidate
				{
				  offset  = OFFSET(s, k, x_dx, y_dy);
				  offset0 = OFFSET(0, 0, x, y);
				  STATS_w(sum_wstats, offset)  += w;
				  STATS_w2(sum_wstats, offset) += w * w;
				  for (i = 2; i < nb_stats; ++i)
				    sum_wstats[offset + i] += w * map_stats[offset0 + i];
				}
			      }
			  }
		      }
		  }
		if (verbose)
#pragma omp critical
		  {
		    percent = (100 * *gl_cpt / Wmax) / nb_windows;
		    if (percent != (100 * (*gl_cpt-1) / Wmax) / nb_windows)
		      sarwaitbar_update(hwaitbar, percent);
		  }
	      }
	    // Aggregations
	    if (cs >= hWmin)
	      for (s = 0; s < S; ++s)
		for (k = 0; k < K; k += hStep)
		  {
		    hP = k+hPmin;
		    for (x = 0; x < M; ++x)
		      for (y = 0; y < N; ++y)
			{
			  offset = OFFSET(s, k, x, y);

			  sum_w  = STATS_w(sum_wstats, offset);
			  sum_w2 = STATS_w2(sum_wstats, offset);

			  // Statistics on matrices
			  frob2 = 0;
			  span2 = 0;
			  for (i = 0; i < D; ++i)
			    {
			      value = STATS_wdiag(sum_wstats, offset, D, i);
			      frob2 += value * value;
			      span2 += value;
			    }
			  for (i = 0; i < D; ++i)
			    for (j = 0; j < i; ++j)
			      {
				value = cabsf(STATS_woffdiag(sum_wstats, offset, D, i, j));
				frob2 += 2 * value * value;
			      }
			  span2 *= span2;
			  span2 /= sum_w * sum_w;
			  frob2 /= sum_w * sum_w;

			  // Check gamma statistics on diagonal
			  alpha = 0;
			  for (i = 0; i < D; ++i)
			    {
			      value  = STATS_wdiag(sum_wstats, offset, D, i) / sum_w;
			      value *= value;
			      alpha2 = compute_alpha(STATS_wdiag2(sum_wstats, offset, i) / sum_w - value,
						     value * eta2);
			      alpha  = MAX(alpha, alpha2);
			    }

			  // Check Wishart statistics
			  alpha2 = compute_alpha(STATS_wspan2(sum_wstats, offset) / sum_w - span2,
						 frob2 * eta2);
			  alpha  = MAX(alpha, alpha2);

			  alpha2 = compute_alpha(STATS_wfrob2(sum_wstats, offset) / sum_w - frob2,
						 span2 * eta2);
			  alpha  = MAX(alpha, alpha2);

			  // Check finite difference Wishart statistics (x direction)
			  alpha2 = compute_alpha(STATS_wspanfdx2(sum_wstats, offset) / sum_w,
						 frob2 * zeta2);
			  alpha3 = compute_alpha(STATS_wspanbdx2(sum_wstats, offset) / sum_w,
						 frob2 * zeta2);
			  alpha = MAX(alpha, MIN(alpha2, alpha3));

			  // Check finite difference Wishart statistics (y direction)
			  alpha2 = compute_alpha(STATS_wspanfdy2(sum_wstats, offset) / sum_w,
						 frob2 * zeta2);
			  alpha3 = compute_alpha(STATS_wspanbdy2(sum_wstats, offset) / sum_w,
						 frob2 * zeta2);
			  alpha = MAX(alpha, MIN(alpha2, alpha3));

			  // LLMMSE update
			  clook = 1. /
			    ((1 - alpha) * (1 - alpha) * (sum_w2 - 1) / (sum_w * sum_w) +
			     ((1 - alpha) / sum_w + alpha) * ((1 - alpha) / sum_w + alpha));

			  // Keep if equivalent number of look has increased
			  if ((FLTDATA_ACCESS(outlook, x, y, 0) < clook))
			    {
			      FLTDATA_ACCESS(outlook, x, y, 0) = clook;
			      FLTDATA_ACCESS(outhW,   x, y, 0) = cs;
			      FLTDATA_ACCESS(outhP,   x, y, 0) = hP;
			      FLTDATA_ACCESS(outS,    x, y, 0) = s;
			      FLTDATA_ACCESS(outG,    x, y, 0) = g;
			      FLTDATA_ACCESS(outalpha,    x, y, 0) = alpha;
			      for (i = 0; i < D; ++i)
				for (j = 0; j <= i; ++j)
				  if (i == j)
				    SARDATA_ACCESS(output, x, y, i, i) =
				      (1-alpha) * STATS_wdiag(sum_wstats, offset, D, i) / sum_w +
				      alpha * SARDATA_ACCESS(input, x, y, i, i);
				  else
				    SARDATA_ACCESS(output, x, y, i, j) =
				      (1-alpha) * STATS_woffdiag(sum_wstats, offset, D, i, j) / sum_w +
				      alpha * SARDATA_ACCESS(input, x, y, i, j);
			    }
			}
		  }
	  }
	free(diff);
      }
      for (x = 0; x < M; ++x)
	for (y = 0; y < N; ++y)
	  for (i = 0; i < D; ++i)
	    for (j = 0; j < i; ++j)
	      SARDATA_ACCESS(output, x, y, j, i) = conjf(SARDATA_ACCESS(output, x, y, i, j));
      free(map_stats);
      free(sum_wstats);
      for (s = 0; s < S; ++s)
	simfunc[g]->free(guidesarsim[s]);
      free(guidesarsim);
      for (k = 0; k < K; k += hStep)
	free(weights_spatial[k]);
      free(weights_spatial);
    }
  return output;
}

static sardata* suppress_zero(sardata* data)
{
  int i, j, k, l;
  double min = INFINITY;
  double minp = INFINITY;
  double value;
  for (i = 0; i < data->M; ++i)
    for (j = 0; j < data->N; ++j)
      for (k = 0; k < data->D; ++k)
	{
	  value = cabsf(SARDATA_ACCESS(data, i, j, k, k));
	  if (value < min)
	    min = value;
	  if (0 < value && value < minp)
	    minp = value;
	}
  if (min == 0.0)
    for (i = 0; i < data->M; ++i)
      for (j = 0; j < data->N; ++j)
	for (k = 0; k < data->D; ++k)
	  if (cabsf(SARDATA_ACCESS(data, i, j, k, k)) == min)
	    {
	      for (l = 0; l < data->D; ++l)
		{
		  SARDATA_ACCESS(data, i, j, k, l) = 0;
		  SARDATA_ACCESS(data, i, j, l, k) = 0;
		}
	      SARDATA_ACCESS(data, i, j, k, k) = minp;
	    }
  return data;
}

static sardata* nlsar(const sardata*		input,
		      sardata*			output,
		      const sarsimstats* const	statssim,
		      sardata** const		guides,
		      sarsimstats** const	gstatssim,
		      int			G,
		      float			rho,
		      float			hphi,
		      int			verbose,
		      fltdata*       		outlook,
		      fltdata*       		outhW,
		      fltdata*       		outhP,
		      fltdata*       		outS,
		      fltdata*       		outG,
		      fltdata*       		outalpha)
{
  const sarsimfuncs** simfunc;
  int M = input->M;
  int N = input->N;
  int D = input->D;
  int TN = M;
  int TM = N;
  int ext;
  int x, y;
  int w, h;
  int x_ext, y_ext;
  int w_ext, h_ext;
  int dx, dy;
  int k, l, m;
  int i, j;
  int g;
  sardata* input_tmp;
  sardata** guides_tmp;
  sardata* output_tmp   = NULL;
  fltdata* outlook_tmp  = NULL;
  fltdata* outhW_tmp    = NULL;
  fltdata* outhP_tmp    = NULL;
  fltdata* outS_tmp     = NULL;
  fltdata* outG_tmp     = NULL;
  fltdata* outalpha_tmp = NULL;
  int gl_cpt = 0;
  int alloc_outlook  = outlook  != NULL;
  int alloc_outhW    = outhW    != NULL;
  int alloc_outhP    = outhP    != NULL;
  int alloc_outS     = outS     != NULL;
  int alloc_outG     = outG     != NULL;
  int alloc_outalpha = outalpha != NULL;
  void* hwaitbar = NULL;

  ext = 0;
  for (g = 0; g < G; ++g)
    ext = MAX(ext, gstatssim[g]->hPmax+gstatssim[g]->hWmax+3);
  while (TM * TN * D > 1024*1024)
    {
      if (TM > TN)
	TM /= 2;
      else
	TN /= 2;
    }
#ifdef OMP
  int nb_procs;
  float s_nb_procs;
  nb_procs = omp_get_max_threads();
  s_nb_procs = floorf(sqrtf(nb_procs));
  TM = ceilf(MIN(TM, M) / s_nb_procs);
  TN = ceilf(MIN(TN, N) / (nb_procs / s_nb_procs));
#endif //!OMP
  TM = MAX(TM, 32);
  TN = MAX(TN, 32);
  if (verbose)
#ifdef OMP
    sarprintf("Computation (#proc=%d, #windows=%d)\n", nb_procs, ((M-1)/TM+1)*((N-1)/TN+1));
#else
    sarprintf("Computation (#proc=%d, #windows=%d)\n", 1, ((M-1)/TM+1)*((N-1)/TN+1));
#endif
  output   = sardata_realloc_size(output,   M, N, D);
  outlook  = fltdata_realloc_size(outlook,  M, N, 1);
  outhW    = fltdata_realloc_size(outhW,    M, N, 1);
  outhP    = fltdata_realloc_size(outhP,    M, N, 1);
  outS     = fltdata_realloc_size(outS,     M, N, 1);
  outG     = fltdata_realloc_size(outG,     M, N, 1);
  outalpha = fltdata_realloc_size(outalpha, M, N, 1);

  simfunc = malloc(G * sizeof(sarsimfuncs*));
  for (g = 0; g < G; ++g)
    switch (gstatssim[g]->simtype)
      {
	case simtype_glr:
	  simfunc[g] = &sarsim_glrwishart;
	  break;
	case simtype_kl:
	  simfunc[g] = &sarsim_klwishart;
	  break;
	case simtype_skl:
	  simfunc[g] = &sarsim_sklwishart;
	  break;
	case simtype_geo:
	  simfunc[g] = &sarsim_geowishart;
	  break;
	default:
	  sarerror_msg("Unknown similarity criterion");
	  return NULL;
      }

  if (verbose)
    hwaitbar = sarwaitbar_open();
#pragma omp parallel default(shared) private(k,l,m,x,y,w,h,x_ext,y_ext,w_ext,h_ext,dx,dy,i,j,input_tmp,guides_tmp,output_tmp,outlook_tmp,outhW_tmp,outhP_tmp,outS_tmp,outG_tmp,outalpha_tmp)
  {
# pragma omp for schedule(dynamic) nowait
    for (m = 0; m < ((M-1)/TM+1) * ((N-1)/TN+1); ++m)
      {
	k = m / ((N-1)/TN+1);
	l = m % ((N-1)/TN+1);

	x = MAX(0, k * TM);
	y = MAX(0, l * TN);
	w = MIN(x + TM, M) - x;
	h = MIN(y + TN, N) - y;
	x_ext = MAX(0, x - ext);
	y_ext = MAX(0, y - ext);
	w_ext = MIN(x + w + ext, M) - x_ext;
	h_ext = MIN(y + h + ext, N) - y_ext;
	input_tmp  = sardata_calloc_size(TM + 2 * ext, TN + 2 * ext, D);
	input_tmp  = sardata_extract(input, input_tmp,
				     x_ext, y_ext,
				     w_ext, h_ext, 1);
	input_tmp  = suppress_zero(input_tmp);
	guides_tmp = malloc(G * sizeof(sardata*));
#pragma omp critical
	for (g = 0; g < G; ++g)
	  {
	    guides_tmp[g]  = sardata_calloc_size(TM + 2 * ext, TN + 2 * ext, D);
	    guides_tmp[g]  = sardata_extract(guides[g], guides_tmp[g],
					     x_ext, y_ext,
					     w_ext, h_ext, 1);
	    guides_tmp[g]  = suppress_zero(guides_tmp[g]);
	  }
	output_tmp   = sardata_calloc_size(TM + 2 * ext, TN + 2 * ext, D);
	outlook_tmp  = fltdata_calloc_size(TM + 2 * ext, TN + 2 * ext, 1);
	outhW_tmp    = fltdata_calloc_size(TM + 2 * ext, TN + 2 * ext, 1);
	outhP_tmp    = fltdata_calloc_size(TM + 2 * ext, TN + 2 * ext, 1);
	outS_tmp     = fltdata_calloc_size(TM + 2 * ext, TN + 2 * ext, 1);
	outG_tmp     = fltdata_calloc_size(TM + 2 * ext, TN + 2 * ext, 1);
	outalpha_tmp = fltdata_calloc_size(TM + 2 * ext, TN + 2 * ext, 1);
	output_tmp = nlsar_core(input_tmp, output_tmp, statssim, guides_tmp,
				simfunc, gstatssim, G, rho, hphi, verbose,
				outlook_tmp, outhW_tmp, outhP_tmp, outS_tmp, outG_tmp, outalpha_tmp,
				&gl_cpt, G*((M-1)/TM+1)*((N-1)/TN+1), hwaitbar);
	for (dx = 0; dx < w; ++dx)
	  for (dy = 0; dy < h; ++dy)
	    {
	      FLTDATA_ACCESS(outlook, x + dx, y + dy, 0) =
		FLTDATA_ACCESS(outlook_tmp, x - x_ext + dx, y - y_ext + dy, 0);
	      FLTDATA_ACCESS(outhW, x + dx, y + dy, 0) =
		FLTDATA_ACCESS(outhW_tmp, x - x_ext + dx, y - y_ext + dy, 0);
	      FLTDATA_ACCESS(outhP, x + dx, y + dy, 0) =
		FLTDATA_ACCESS(outhP_tmp, x - x_ext + dx, y - y_ext + dy, 0);
	      FLTDATA_ACCESS(outS, x + dx, y + dy, 0) =
		FLTDATA_ACCESS(outS_tmp, x - x_ext + dx, y - y_ext + dy, 0);
	      FLTDATA_ACCESS(outG, x + dx, y + dy, 0) =
		FLTDATA_ACCESS(outG_tmp, x - x_ext + dx, y - y_ext + dy, 0);
	      FLTDATA_ACCESS(outalpha, x + dx, y + dy, 0) =
		FLTDATA_ACCESS(outalpha_tmp, x - x_ext + dx, y - y_ext + dy, 0);
	      for (i = 0; i < D; ++i)
		for (j = 0; j < D; ++j)
		  SARDATA_ACCESS(output, x + dx, y + dy, i, j) =
		    SARDATA_ACCESS(output_tmp, x - x_ext + dx, y - y_ext + dy, i, j);
	    }
	sardata_free(input_tmp);
	for (g = 0; g < G; ++g)
	  {
	    sardata_free(guides_tmp[g]);
	  }
	free(guides_tmp);
	sardata_free(output_tmp);
	fltdata_free(outlook_tmp);
	fltdata_free(outhW_tmp);
	fltdata_free(outhP_tmp);
	fltdata_free(outS_tmp);
	fltdata_free(outG_tmp);
	fltdata_free(outalpha_tmp);
      }
  }
  if (!alloc_outlook)
    fltdata_free(outlook);
  if (!alloc_outhW)
    fltdata_free(outhW);
  if (!alloc_outhP)
    fltdata_free(outhP);
  if (!alloc_outG)
    fltdata_free(outG);
  if (!alloc_outS)
    fltdata_free(outS);
  if (!alloc_outalpha)
    fltdata_free(outalpha);
  if (verbose)
    sarwaitbar_close(hwaitbar);
  free(simfunc);
  return output;
}

sardata* sarnlsar(const sardata*		input,
		  sardata*			output,
		  const sarsimstats*		statssim,
		  sardata** const		guides,
		  sarsimstats** const		gstatssim,
		  int				G,
		  int				n_args,
		  ...)
{
  int verbose = 1;
  float rho = 1.0;
  float h = 1.0;
  fltdata* outlook  = NULL;
  fltdata* outhW    = NULL;
  fltdata* outhP    = NULL;
  fltdata* outS     = NULL;
  fltdata* outG     = NULL;
  fltdata* outalpha = NULL;
  int i;
  va_list ap;

  va_start(ap, n_args);
  for (i = 0; i < n_args; i++)
    switch (i)
      {
	case 0:
	  verbose = va_arg(ap, int);
	  break;
	case 1:
	  rho = va_arg(ap, double);
	  break;
	case 2:
	  h = va_arg(ap, double);
	  break;
	case 3:
	  outlook = va_arg(ap, fltdata*);
	  break;
	case 4:
	  outhW = va_arg(ap, fltdata*);
	  break;
	case 5:
	  outhP = va_arg(ap, fltdata*);
	  break;
	case 6:
	  outS = va_arg(ap, fltdata*);
	  break;
	case 7:
	  outG = va_arg(ap, fltdata*);
	  break;
	case 8:
	  outalpha = va_arg(ap, fltdata*);
	  break;
	default:
	  sarerror_msg("Too many arguments");
	  return NULL;
      }
  va_end(ap);
  if (rho < 0)
    sarerror_msg("Parameter rho must be a positive floating number");
  if (h < 0)
    sarerror_msg("Parameter h must be a positive floating number");

  output = nlsar(input, output, statssim, guides, gstatssim, G,
		 rho, h, verbose,
		 outlook, outhW, outhP, outS, outG, outalpha);
  if (!output)
    {
      sarerror_msg_msg("Cannot filter the image");
      return NULL;
    }

  return output;
}
