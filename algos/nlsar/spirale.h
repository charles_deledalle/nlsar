#ifndef SPIRALE_H_
# define SPIRALE_H_

typedef int int2[2];

void spirale(int hW, int2* shifts, int* lengths, int hStep);

#endif // !SPIRALE_H_
