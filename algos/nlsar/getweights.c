/*
** nlsar.c: implementation of the NL-SAR filter
**
** This file is part of NL-SAR Toolbox version 0.9.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:46:54 2013 Charles-Alban Deledalle
** Last update Wed Jul 20 23:26:59 2016 Charles-Alban Deledalle
*/

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#ifdef OMP
# include <omp.h>
#endif //!OMP
#include "tools/sarprintf.h"
#include "tools/sarwaitbar.h"
#include "data/sardata.h"
#include "tools/sarerror.h"
#include "tools/mathtools.h"
#include "tools/matrixtools.h"
#include "algos/carfilter/carfilter.h"
#include "algos/noisegen/noisegen.h"
#include "sarsim.h"
#include "spirale.h"
#include "sarsimstats.h"
#include "nlsar.h"
#include "phi.h"

static fltdata* getweights_core(const sardata*			input,
				fltdata*			outweights,
				const sarsimfuncs*		simfunc,
				const sarsimstats* const	stats,
				int				x,
				int				y,
				int hW, int hP, int S, float alpha)
{
  sardata*	binput;
  sarsimdata*	inputsarsim;
  int M = input->M;
  int N = input->N;
  int D = input->D;
  int W, l;
  int i, j;
  int k, s;
  int dx, dy, x_dx, y_dy;
  float d, sum_w, w;
  float L = stats->L;

  k = hP - stats->hPmin;

  { // Allocations
    binput = sardata_dup(input);
    if (S)
      binput = sargausscar(input, binput, S);
    if (!(inputsarsim = simfunc->create(L, binput)))
      return NULL;
    sardata_free(binput);
  }

  sum_w = 0;
  {
    // Central pixel
    for (i = 0; i < 2*hW+1; ++i)
      for (j = 0; j < 2*hW+1; ++j)
	FLTDATA_ACCESS(outweights, i, j, 0) = 0;
    FLTDATA_ACCESS(outweights, hW, hW, 0) = 1;
    sum_w += 1;

    // Other pixels
    W = stats->spirale_lengths[hW];
    for (s = -1; s <= 1; s += 2)
      for (l = 0; l < W-1; ++l)
	{
	  dx = s*stats->spirale_shifts[l+1][0];
	  dy = s*stats->spirale_shifts[l+1][1];

	  x_dx = MOD(x + dx, M);
	  y_dy = MOD(y + dy, N);

	  // Extract weights for current patch size
	  d = 0;
	  for (i = -hP; i <= hP; ++i)
	    for (j = -hP; j <= hP; ++j)
	      d +=
		simfunc->lsarsim(D,
				 SARSIMDATA_ACCESS(inputsarsim,
						   MOD(x + i, M), MOD(y + j, N)),
				 SARSIMDATA_ACCESS(inputsarsim,
						   MOD(x_dx + i, M), MOD(y_dy + j, N)));
	  w = phi(d, SARSIMSTATS_ACCESS(stats, S, k, l), 1.0);
	  w *= expf(-(dx*dx+dy*dy)/(2*hP+1.5)/(2*hP+1.5));
	  if ((0 <= hW + dx && hW + dx < 2*hW + 1) &&
	      (0 <= hW + dy && hW + dy < 2*hW + 1))
	    {
	      FLTDATA_ACCESS(outweights, hW + dx, hW + dy, 0) = w;
	      sum_w += w;
	    }
	}
  }
  for (i = 0; i < 2*hW+1; ++i)
    for (j = 0; j < 2*hW+1; ++j)
      FLTDATA_ACCESS(outweights, i, j, 0) *= (1 - alpha) / sum_w;
  FLTDATA_ACCESS(outweights, hW, hW, 0) += alpha;
  simfunc->free(inputsarsim);
  return outweights;
}

fltdata* sargetweights(const sardata* input,
		       fltdata* outweights,
		       const sarsimstats* const		stats,
		       int x, int y,
		       int hW, int hP, int S, float alpha)
{
  sardata* subinput;
  int x1, y1, x2, y2, w, h, hWmax;

  if (!(0 <= x && x <= input->M && 0 <= y && y <= input->N))
    {
      sarerror_msg("Index out of bounds");
      return NULL;
    }
  hWmax = stats->hWmax;
  x1 = MAX(x - (2*hWmax+1), 0);
  x2 = MIN(x + (2*hWmax+1), input->M - 1);
  y1 = MAX(y - (2*hWmax+1), 0);
  y2 = MIN(y + (2*hWmax+1), input->N - 1);
  w = x2 - x1 + 1;
  h = y2 - y1 + 1;
  subinput   = sardata_alloc_size(w, h, input->D);
  subinput   = sardata_extract(input, subinput,
			       x1, y1, w, h, 1);
  outweights = fltdata_realloc_size(outweights, 2*hW+1, 2*hW+1, 1);
  outweights = getweights_core(subinput, outweights, &sarsim_glrwishart, stats,
			       x - x1, y - y1,
			       hW, hP, S, alpha);
  sardata_free(subinput);
  if (!outweights)
    {
      sarerror_msg_msg("Cannot compute the weights");
      return NULL;
    }

  return outweights;
}
