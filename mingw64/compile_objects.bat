@echo on

cd ..

gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/nlsar/nlsar.c -o algos/nlsar/nlsar.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c tools/sarerror.c -o tools/sarerror.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c tools/sarprintf.c -o tools/sarprintf.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c tools/sarwaitbar.c -o tools/sarwaitbar.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c tools/matrixtools.c -o tools/matrixtools.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c tools/inv_func.c -o tools/inv_func.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/iorat.c -o data/iorat.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/iobin.c -o data/iobin.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/ioxima.c -o data/ioxima.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/iosar.c -o data/iosar.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/ionetpbm.c -o data/ionetpbm.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/sardata.c -o data/sardata.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/rgbdata.c -o data/rgbdata.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/fltdata.c -o data/fltdata.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c data/sar2rgb.c -o data/sar2rgb.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/carfilter/sarboxcar.c -o algos/carfilter/sarboxcar.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/carfilter/sardiskcar.c -o algos/carfilter/sardiskcar.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/carfilter/sargausscar.c -o algos/carfilter/sargausscar.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/carfilter/fltboxcar.c -o algos/carfilter/fltboxcar.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/carfilter/fltdilate.c -o algos/carfilter/fltdilate.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/nlsar/sarsimstats.c -o algos/nlsar/sarsimstats.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/nlsar/sarsim_glrwishart.c -o algos/nlsar/sarsim_glrwishart.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/nlsar/sarsim_klwishart.c -o algos/nlsar/sarsim_klwishart.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/nlsar/sarsim_geowishart.c -o algos/nlsar/sarsim_geowishart.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/nlsar/phi.c -o algos/nlsar/phi.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/nlsar/spirale.c -o algos/nlsar/spirale.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/nlsar/getweights.c -o algos/nlsar/getweights.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/noisegen/noisegen.c -o algos/noisegen/noisegen.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c algos/noiseest/noiseest.c -o algos/noiseest/noiseest.o

cd mingw64