@echo on

cd ..

md build\MinGW-64\cli 2>NUL
copy interfaces\cli\*.exe  build\MinGW-64\cli
md build\MinGW-64\lib 2>NUL
copy lib\*.dll build\MinGW-64\lib
copy lib\*.a build\MinGW-64\lib
md build\MinGW-64\mex 2>NUL
copy interfaces\mex\*.m build\MinGW-64\mex
copy interfaces\mex\*.mexw64 build\MinGW-64\mex
copy interfaces\mex\*.fig build\MinGW-64\mex

cd mingw64